Categories:Office
License:Apache2
Web Site:https://github.com/Kyakujin/TagNotepad
Source Code:https://github.com/Kyakujin/TagNotepad
Issue Tracker:https://github.com/Kyakujin/TagNotepad/issues

Auto Name:TAG Notepad
Summary:Tag-based notebook
Description:
No description available
.

Repo Type:git
Repo:https://github.com/Kyakujin/TagNotepad.git

Build:1.0.2,3
    commit=5f68
    srclibs=1:ActionBarSherlock@4.4.0

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.2
Current Version Code:3

