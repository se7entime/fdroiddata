Categories:Multimedia
License:Apache2
Web Site:https://github.com/anupam1525/AcrylicPaint
Source Code:https://github.com/anupam1525/AcrylicPaint
Issue Tracker:https://github.com/anupam1525/AcrylicPaint/issues

Auto Name:Acrylic Paint
Summary:Simple finger painting
Description:
Acrylic Paint is a coloring tool based on the FingerPaint project
taken from API demos.
.

Repo Type:git
Repo:https://github.com/anupam1525/AcrylicPaint.git

Build:1.2.0,3
    commit=e534db825b557ac523297599139a0616a4ce2545
    subdir=Acrylic_Paint

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2.0
Current Version Code:3

