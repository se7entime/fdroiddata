Categories:System
License:Apache2
Web Site:https://github.com/segin/TTLEditor
Source Code:https://github.com/segin/TTLEditor
Issue Tracker:https://github.com/segin/TTLEditor/issues

Auto Name:TTL Editor
Summary:Change TTL of networking packets
Description:
Simple graphical frontend for iptables to change the TTL (time-to-live) of
packets sent over a given network interface.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/segin/TTLEditor

Build:1.0.8,10
    commit=8d3f9da25b16c79fbe6f9977db83f5f64deceaf2
    subdir=app
    gradle=yes
    srclibs=RootTools@3.4
    rm=app/libs/RootTools.jar
    prebuild=pushd $$RootTools$$/RootTools && \
        gradle makejar && \
        popd && \
        cp $$RootTools$$/RootTools/build/bundles/release/classes.jar libs/roottools-3.4.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.8
Current Version Code:10

