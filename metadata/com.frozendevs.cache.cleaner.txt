Categories:System
License:MIT
Web Site:http://www.frozendevs.com
Source Code:https://github.com/Frozen-Developers/android-cache-cleaner
Issue Tracker:https://github.com/Frozen-Developers/android-cache-cleaner/issues

Auto Name:Cache Cleaner
Summary:Clean the cache
Description:
Cache cleaning tool which does not require a rooted device. Features
native Holo look and feel and fast operation.
.

Repo Type:git
Repo:https://github.com/Frozen-Developers/android-cache-cleaner

Build:1.1.7,10
    commit=9cf2c6030617570f343c85b471f2b840e896cea0
    subdir=CacheCleaner
    gradle=main

Build:1.1.8,11
    commit=v1.1.8
    subdir=CacheCleaner
    gradle=main

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.8
Current Version Code:11

