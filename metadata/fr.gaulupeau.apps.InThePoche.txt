Categories:Internet
License:GPLv3
Web Site:http://www.inthepoche.com
Source Code:https://github.com/wallabag/android-app
Issue Tracker:https://github.com/wallabag/android-app/issues
FlattrID:1265480

Auto Name:In the poche
Summary:Read-it-later client
Description:
Client for the self hosted read-it-later web app Poche aka Wallabag.
.

Repo Type:git
Repo:https://github.com/wallabag/android-app.git

Build:1.5.3.2,8
    commit=1.5.3.2

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.5.3.2
Current Version Code:8

