Categories:System
License:MIT
Web Site:https://github.com/mick88/filemanager
Source Code:https://github.com/mick88/filemanager
Issue Tracker:https://github.com/mick88/filemanager/issues

Auto Name:File Manager Pro
Summary:File manager
Description:
A file manager.
.

Repo Type:git
Repo:https://github.com/mick88/filemanager.git

Build:0.3,5
    commit=c1b37448572a1e5244fd89abe1eeec9956c11560
    target=android-19
    rm=libs/msqlite.jar,libs/SystemBarTint.jar,libs/android-support-v4.jar
    extlibs=android/android-support-v4.jar
    srclibs=1:MSQLite@4f4a7ce66332e3576a1ef993141d01dc9af2fe64,2:SystemBarTint@v1.0.3

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.3
Current Version Code:5

