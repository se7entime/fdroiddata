Categories:Development
License:GPLv3
Web Site:http://blog.ecomobilecitizen.com/android/bluetoothviewer
Source Code:https://github.com/janosgyerik/bluetoothviewer
Issue Tracker:https://github.com/janosgyerik/bluetoothviewer/issues
Donate:https://github.com/janosgyerik/bluetoothviewer#Donations

Auto Name:Bluetooth Viewer (LITE)
Summary:Bluetooth connection debugging tool
Description:
* Connect to any Bluetooth device
* Display incoming raw data
* Send raw data to the Bluetooth device
* You can confirm successful pairing, monitor incoming raw data and see exactly what is being transmitted from the Bluetooth device
.

Repo Type:git
Repo:https://github.com/janosgyerik/bluetoothviewer.git

Build:1.0.1,2
    commit=04e06b866d0
    rm=scripts,samples,graphics

Build:1.0.2,3
    commit=24167d735f46fa4ab14c867f6fd71bd17fc36171
    rm=scripts,samples,graphics
    gradle=yes
    subdir=bluetoothviewer

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.2
Current Version Code:3

